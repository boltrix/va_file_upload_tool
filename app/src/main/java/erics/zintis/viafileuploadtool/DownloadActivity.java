package erics.zintis.viafileuploadtool;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.HashMap;

import erics.zintis.viafileuploadtool.utils.FirebaseFile;

public class DownloadActivity extends AppCompatActivity {
    private HashMap<String, FirebaseFile> registry = new HashMap<String, FirebaseFile>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download);
        FirebaseFile.Synchronize(firebaseFile -> {
            registry.put(firebaseFile.name, firebaseFile);
            // TODO add to list
        }, firebaseFile -> {
            registry.put(firebaseFile.name, firebaseFile);
            // TODO change list entry
        }, firebaseFile -> {
            registry.remove(firebaseFile.name);
            // TODO remove from list
        }, databaseError -> {
            // TODO handle error
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }
}
