package erics.zintis.viafileuploadtool;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.OnProgressListener;

import erics.zintis.viafileuploadtool.utils.FirebaseFile;

public class UploadActivity extends AppCompatActivity implements OnFailureListener, OnProgressListener<Double>, OnSuccessListener<FirebaseFile> {
    private static final int PICK_A_FILE_REQUEST_CODE = 1;

    private FirebaseFile upload = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // save our upload
        if (upload != null) {
            outState.putParcelable("upload", upload);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        // get potential upload
        Object obj = savedInstanceState.getParcelable("upload");
        if (obj instanceof FirebaseFile){
            // got upload, restore it
            upload = (FirebaseFile) obj;
            upload.restoreUpload(this, this, this);
        }
    }

    public void pickFile(View view) {
        // open any file
        // TODO limit to zip
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.setType("*/*");
        // start file picker
        startActivityForResult(intent, PICK_A_FILE_REQUEST_CODE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        // process file picker result
        if (requestCode == PICK_A_FILE_REQUEST_CODE && resultCode == RESULT_OK && resultData != null) {
            // result ok, upload file selected
            upload = new FirebaseFile(resultData.getData());
            upload.upload(this, this, this);
        }
    }

    @Override
    public void onFailure(@NonNull Exception e) {
        // TODO handle exception
        Log.e("viafileuploadtool", "ouch", e);
        upload = null;
    }

    @Override
    public void onProgress(@NonNull Double progress) {
        // TODO progress bar
        Log.d("viafileuploadtool", progress.toString());
    }

    @Override
    public void onSuccess(@NonNull FirebaseFile firebaseFile) {
        // TODO done
        Log.d("viafileuploadtool", firebaseFile.remoteFile.toString());
        upload = null;
    }
}
