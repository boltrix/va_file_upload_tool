package erics.zintis.viafileuploadtool.utils;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.UploadTask;

import java.util.HashMap;
import java.util.function.Consumer;

public class FirebaseFile implements Parcelable {
    private static final FirebaseStorage STORAGE = FirebaseStorage.getInstance();
    // reference to our remote file storage folder
    private static final StorageReference STORAGE_REFERENCE = STORAGE.getReference("viafileuploadtool");
    // reference to our remote db table
    private static final DatabaseReference DATABASE_REFERENCE = FirebaseDatabase.getInstance().getReference("viafileuploadtool");
    // our remote db synchronizer
    private static FirebaseFileSynchronizer SYNCHRONIZER = null;

    // synchronizes us to remote db
    private static final class FirebaseFileSynchronizer implements ChildEventListener {
        // callbacks for events
        private Consumer<FirebaseFile> onChildAddedCallback = null;
        private Consumer<FirebaseFile> onChildChangedCallback = null;
        private Consumer<FirebaseFile> onChildRemovedCallback = null;
        private Consumer<DatabaseError> onCancelledCallback = null;
        // our known files
        private HashMap<String, FirebaseFile> registry = new HashMap<String, FirebaseFile>();

        public void setCallbacks(@NonNull Consumer<FirebaseFile> onChildAdded, @NonNull Consumer<FirebaseFile> onChildChanged, @NonNull Consumer<FirebaseFile> onChildRemoved, @NonNull Consumer<DatabaseError> onCancelled) {
            // setup callbacks
            onChildAddedCallback = onChildAdded;
            onChildChangedCallback = onChildChanged;
            onChildRemovedCallback = onChildRemoved;
            onCancelledCallback = onCancelled;
        }

        @Override
        public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            // check if we already know this one
            String key = dataSnapshot.getKey();
            FirebaseFile firebaseFile = registry.get(key);
            if (firebaseFile == null) {
                // we don't, get it and update registry
                firebaseFile = dataSnapshot.getValue(FirebaseFile.class);
                registry.put(key, firebaseFile);
            }
            onChildAddedCallback.accept(firebaseFile);
        }

        @Override
        public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            // get firebaseFile and update registry
            FirebaseFile firebaseFile = dataSnapshot.getValue(FirebaseFile.class);
            registry.put(dataSnapshot.getKey(), firebaseFile);
            onChildChangedCallback.accept(firebaseFile);
        }

        @Override
        public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
            // check if we already know this one
            String key = dataSnapshot.getKey();
            FirebaseFile firebaseFile = registry.get(key);
            if (firebaseFile == null) {
                // we don't, just get it
                onChildRemovedCallback.accept(dataSnapshot.getValue(FirebaseFile.class));
            } else {
                // we do, remove in from registry
                registry.remove(key);
                onChildRemovedCallback.accept(firebaseFile);
            }
        }

        @Override
        public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            // something got moved, we ignore this(what are we supposed to do in this case?)
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
            // some event got cancelled
            onCancelledCallback.accept(databaseError);
        }
    }

    // creates us from parcel
    public static final Parcelable.Creator<FirebaseFile> CREATOR = new Parcelable.Creator<FirebaseFile>() {
        public FirebaseFile[] newArray(int size) {
            // create our array
            return new FirebaseFile[size];
        }

        public FirebaseFile createFromParcel(Parcel in) {
            // restore us
            return new FirebaseFile(in);
        }
    };

    public static void Synchronize(@NonNull Consumer<FirebaseFile> onChildAdded, @NonNull Consumer<FirebaseFile> onChildChanged, @NonNull Consumer<FirebaseFile> onChildRemoved, @NonNull Consumer<DatabaseError> onCancelled) {
        // initialize SYNCHRONIZER
        if (SYNCHRONIZER == null) {
            SYNCHRONIZER = new FirebaseFileSynchronizer();
        }
        // set callbacks and listen
        SYNCHRONIZER.setCallbacks(onChildAdded, onChildChanged, onChildRemoved, onCancelled);
        DATABASE_REFERENCE.addChildEventListener(SYNCHRONIZER);
    }

    // uri to file in local storage
    private Uri localFile = null;
    // reference to file in remote storage
    private StorageReference storageReference = null;
    // reference to record in remote db
    private DatabaseReference databaseReference = null;

    // our file name/pk
    public String name = null;
    // uri to file in remote storage
    public Uri remoteFile = null;

    private FirebaseFile(Parcel in) {
        // restore localFile uri from parcel string
        this(Uri.parse(in.readString()));
    }

    public FirebaseFile(@NonNull Uri uri) {
        // our local file for later use
        localFile = uri;
        // it's name is used as pk
        // TODO set name from group components
        name = localFile.getLastPathSegment();
        // reference to file storage
        storageReference = STORAGE_REFERENCE.child(name);
        // reference to db record
        databaseReference = DATABASE_REFERENCE.child(name);
    }

    public FirebaseFile() {
        // required for saving to Firebase Database
    }

    private void addUploadListeners(UploadTask uploadTask, OnFailureListener onFailureListener, OnProgressListener<Double> onProgressListener, OnSuccessListener<FirebaseFile> onSuccessListener) {
        // add onFailureListener
        uploadTask.addOnFailureListener(onFailureListener).addOnProgressListener(taskSnapshot -> {
            // progress happens, invoke onProgressListener with progress percent
            onProgressListener.onProgress(100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
        }).addOnSuccessListener(taskSnapshot -> {
            // successful upload, now get it's remote uri with onFailureListener
            storageReference.getDownloadUrl().addOnFailureListener(onFailureListener).addOnSuccessListener(uri -> {
                // got uri, set it
                remoteFile = uri;
                // and save ourselves to db with onFailureListener
                databaseReference.setValue(this).addOnFailureListener(onFailureListener).addOnSuccessListener(nothing -> {
                    // successful save, invoke onSuccessListener with ourselves
                    onSuccessListener.onSuccess(this);
                });
            });
        });
    }

    public void upload(@NonNull OnFailureListener onFailureListener, @NonNull OnProgressListener<Double> onProgressListener, @NonNull OnSuccessListener<FirebaseFile> onSuccessListener) {
        // upload file with listeners
        addUploadListeners(storageReference.putFile(localFile), onFailureListener, onProgressListener, onSuccessListener);
    }

    public void restoreUpload(@NonNull OnFailureListener onFailureListener, @NonNull OnProgressListener<Double> onProgressListener, @NonNull OnSuccessListener<FirebaseFile> onSuccessListener) {
        // restore uploads in progress with listeners
        for (UploadTask uploadTask : storageReference.getActiveUploadTasks()) {
            addUploadListeners(uploadTask, onFailureListener, onProgressListener, onSuccessListener);
        }
    }

    private void addDownloadListeners(FileDownloadTask fileDownloadTask, OnFailureListener onFailureListener, OnSuccessListener<FirebaseFile> onSuccessListener) {
        // add onFailureListener
        fileDownloadTask.addOnFailureListener(onFailureListener).addOnSuccessListener(taskSnapshot -> {
            // successful download, invoke onSuccessListener with ourselves
            onSuccessListener.onSuccess(this);
        });
    }

    public void download(@NonNull Uri uri, @NonNull OnFailureListener onFailureListener, @NonNull OnSuccessListener<FirebaseFile> onSuccessListener) {
        // initialize storageReference
        if (storageReference == null) {
            storageReference = STORAGE.getReferenceFromUrl(remoteFile.toString());
        }
        // download file with listeners
        addDownloadListeners(storageReference.getFile(uri), onFailureListener, onSuccessListener);
    }

    public void restoreDownload(@NonNull OnFailureListener onFailureListener, @NonNull OnSuccessListener<FirebaseFile> onSuccessListener) {
        // initialize storageReference
        if (storageReference == null) {
            storageReference = STORAGE.getReferenceFromUrl(remoteFile.toString());
        }
        // restore downloads in progress with listeners
        for (FileDownloadTask fileDownloadTask : storageReference.getActiveDownloadTasks()) {
            addDownloadListeners(fileDownloadTask, onFailureListener, onSuccessListener);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        // our localFile uri is all we need
        dest.writeString(localFile.toString());
    }
}
