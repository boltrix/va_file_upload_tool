package erics.zintis.viafileuploadtool;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void upload(View view) {
        startActivity(new Intent(this, UploadActivity.class));
    }

    public void download(View view) {
        startActivity(new Intent(this, DownloadActivity.class));
    }
}
